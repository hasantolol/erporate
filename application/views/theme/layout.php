
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url() ?>assets/layout/images/favicon.png">
    <title>Admin Pro Admin Template - The Ultimate Bootstrap 4 Admin Template</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?= base_url() ?>assets/layout/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= base_url() ?>assets/layout/plugins/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
    <!-- This page CSS -->
    <!-- chartist CSS -->
    <link href="<?= base_url() ?>assets/layout/plugins/chartist-js/dist/chartist.min.css" rel="stylesheet">
    <link href="<?= base_url() ?>assets/layout/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css" rel="stylesheet">
    <!--c3 CSS -->
    <link href="<?= base_url() ?>assets/layout/plugins/c3-master/c3.min.css" rel="stylesheet">
    <!--Toaster Popup message CSS -->
    <link href="<?= base_url() ?>assets/layout/plugins/toast-master/css/jquery.toast.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?= base_url() ?>assets/layout/css/style.css" rel="stylesheet">
    <!-- Dashboard 1 Page CSS -->
    <link href="<?= base_url() ?>assets/layout/css/pages/dashboard1.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="<?= base_url() ?>assets/layout/css/colors/default.css" id="theme" rel="stylesheet"> 
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/layout/plugins/datatables/media/css/dataTables.bootstrap4.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />

</head>

<body class="fix-header fix-sidebar card-no-border"> 
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Admin Pro</p>
        </div>
    </div> 
    <div id="main-wrapper"> 
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-light"> 
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.html">
                        <!-- Logo icon --><b> 
                            <img src="<?= base_url() ?>assets/layout/images/logo-icon.png" alt="homepage" class="dark-logo" />
                            <!-- Light Logo icon -->
                            <img src="<?= base_url() ?>assets/layout/images/logo-light-icon.png" alt="homepage" class="light-logo" />
                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text --><span>
                           <!-- dark Logo text -->
                           <img src="<?= base_url() ?>assets/layout/images/logo-text.png" alt="homepage" class="dark-logo" />
                           <!-- Light Logo text -->    
                           <img src="<?= base_url() ?>assets/layout/images/logo-light-text.png" class="light-logo" alt="homepage" /></span> </a>
                       </div> 
                       <div class="navbar-collapse"> 
                        <ul class="navbar-nav mr-auto">
                            <!-- This is  -->
                            <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                            <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        </ul> 
                        <ul class="navbar-nav my-lg-0">  
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?= base_url() ?>assets/layout/images/users/1.jpg" alt="user" class="profile-pic" /></a>
                                <div class="dropdown-menu dropdown-menu-right animated flipInY">
                                    <ul class="dropdown-user">
                                        <li>
                                            <div class="dw-user-box">
                                                <div class="u-img"><img src="<?= base_url() ?>assets/layout/images/users/1.jpg" alt="user"></div>
                                                <div class="u-text">
                                                    <h4><?=$_SESSION['username'];?></h4> 
                                                </div>
                                            </li> 
                                            <li><a href="<?=base_url('auth/logout');?>"><i class="fa fa-power-off"></i> Logout</a></li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </header> 
                <aside class="left-sidebar">
                    <!-- Sidebar scroll-->
                    <div class="scroll-sidebar">
                        <!-- Sidebar navigation-->
                        <nav class="sidebar-nav">
                            <ul id="sidebarnav">
                                <li><a href="<?=base_url();?>">Dashboard </a></li>
                                <?php if (isset($_SESSION)) {
                                    if ($_SESSION['hak_akses']=='admin') {
                                        ?>
                                        <li><a href="<?=base_url('adm_user');?>">User </a></li>
                                        <?php
                                    }elseif ($_SESSION['hak_akses']=='kasir') {
                                        ?>
                                        <li><a href="<?=base_url('k_pesanan');?>">Pesanan </a></li>
                                        <?php
                                    }elseif ($_SESSION['hak_akses']=='pelayan') {
                                        ?>
                                        <li><a href="<?=base_url('p_pesanan');?>">Pesanan </a></li>
                                        <li><a href="<?=base_url('p_menu');?>">Daftar Menu </a></li>
                                        <?php
                                    }
                                }
                                ?> 
                            </ul>
                        </nav>
                        <!-- End Sidebar navigation -->
                    </div>
                    <!-- End Sidebar scroll-->
                </aside> 
                <div class="page-wrapper"> 
                    <!-- ============================================================== -->
                    <!-- Container fluid  -->
                    <!-- ============================================================== -->
                    <div class="container">
                        <!-- ============================================================== -->
                        <!-- Bread crumb and right sidebar toggle -->
                        <!-- ============================================================== -->
                        <?php isset($TPL_ISI) ? $this->load->view($TPL_ISI) : 'Index belum di-load'; ?> 
                        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button> 

                    </div> 
                    <footer class="footer"> © 2018 Admin Pro by wrappixel.com </footer> 
                </div> 
            </div> 
            <script src="<?= base_url() ?>assets/layout/plugins/jquery/jquery.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
            <!-- Bootstrap popper Core JavaScript -->
            <script src="<?= base_url() ?>assets/layout/plugins/bootstrap/js/popper.min.js"></script>
            <script src="<?= base_url() ?>assets/layout/plugins/bootstrap/js/bootstrap.min.js"></script>
            <!-- slimscrollbar scrollbar JavaScript -->
            <script src="<?= base_url() ?>assets/layout/js/perfect-scrollbar.jquery.min.js"></script>
            <!--Wave Effects -->
            <script src="<?= base_url() ?>assets/layout/js/waves.js"></script>
            <!--Menu sidebar -->
            <script src="<?= base_url() ?>assets/layout/js/sidebarmenu.js"></script>
            <!--Custom JavaScript -->
            <script src="<?= base_url() ?>assets/layout/js/custom.min.js"></script>
            <!-- ============================================================== -->
            <!-- This page plugins -->
            <!-- ============================================================== -->
            <!--sparkline JavaScript -->
            <script src="<?= base_url() ?>assets/layout/plugins/sparkline/jquery.sparkline.min.js"></script>
            <!--morris JavaScript --> 
            <!--c3 JavaScript -->
            <script src="<?= base_url() ?>assets/layout/plugins/d3/d3.min.js"></script>
            <script src="<?= base_url() ?>assets/layout/plugins/c3-master/c3.min.js"></script> 
            <!-- Chart JS -->
            <script src="<?= base_url() ?>assets/layout/js/dashboard1.js"></script>
            <!-- ============================================================== -->
            <!-- Style switcher -->
            <!-- ============================================================== -->
            <script src="<?= base_url() ?>assets/layout/plugins/styleswitcher/jQuery.style.switcher.js"></script>
            <script src="<?= base_url() ?>assets/layout/plugins/datatables/datatables.min.js"></script>
            <?php isset($TPL_FOOTER) ? $this->load->view($TPL_FOOTER) : NULL; ?> 
        </body>

        </html>