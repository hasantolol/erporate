<?php
require_once APPPATH . 'controllers/Core_base.php';

class Adm_user extends Core_base {

	public function __construct() {
		$this->load->model('m_adm_user');
        // Check that the user is logged in
		// if (($this->session->userdata('id_user') == null || $this->session->userdata('id_user') < 1) AND $this->session->userdata('hak_akses') == 'pelayan') {
		if ($this->session->userdata('status') != 'login' AND $this->session->userdata('hak_akses') != 'pelayan') {
            // Prevent infinite loop by checking that this isn't the login controller               
			if ( $this->router->fetch_class() != 'auth') 
			{                        
				redirect(base_url());
			}
		}
		parent::__construct(); 
	}

	public function index() { 
		parent::display('index',NULL,'index_footer'); 
	}
	public function get_user()
	{ 
		// echo "<pre>";
		// print_r($this->input->post());
		if (!$this->input->is_ajax_request()) {
			return;
		}  
		$datatable = $this->m_adm_user->get_user($this->input->post());

		$this->output->set_output(json_encode($datatable));
	} 
	
	public function tambah()
	{
		if (!$this->input->is_ajax_request()) {
			return;
		} 
		$data['id_user'] 		= $this->uuid->v4(true);
		$data['nama'] 			= $this->input->post('nama');
		$data['email'] 			= $this->input->post('email');
		$data['hak_akses'] 		= $this->input->post('hak_akses');
		$data['username'] 		= $this->input->post('username');
		$data['password'] 		= md5($this->input->post('password'));
		$data['created_by']		= $this->session->userdata('id_user');
		$data['created_date']		= date('Y-m-d H:i:s');
		// echo "<pre>";
		// print_r($data);
		// exit;

		if ($this->m_adm_user->tambah($data)) { 

			$result = [
				'status' => true, 'pesan' => 'tambah berhasil'
			];

		}else{

			$result = [
				'status' => false, 'pesan' => 'gagal tambah'
			];
		}
		return $this->output->set_output(json_encode($result));
	}

	public function data_user()
	{ 

        // validasi hanya request lewat ajax
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
        // get data
		$data['data'] = $this->m_adm_user->data_user($this->input->post('data_id')); 

        // validasi jika kosong
		if (empty($data['data'])) {
			return $this->output->set_output(json_encode(array('pesan' => 'data tidak ditemukan!', 'data' => null, 'status' => 0)));
		}
		$data['status'] = true;
		return $this->output->set_output(json_encode($data));

	}

	public function edit()
	{
		if (!$this->input->is_ajax_request()) {
			return;
		} 
		$data['id_user'] 		= $this->input->post('id_user');
		$data['nama'] 			= $this->input->post('nama');
		$data['email'] 			= $this->input->post('email');
		$data['hak_akses'] 		= $this->input->post('hak_akses');
		$data['username'] 		= $this->input->post('username');
		if (!empty($this->input->post('password'))) {
			$data['password'] 		= md5($this->input->post('password'));
		}

		if ($this->m_adm_user->edit($data)) { 

			$result = [
				'status' => true, 'pesan' => 'ubah berhasil'
			];

		}else{

			$result = [
				'status' => false, 'pesan' => 'gagal ubah'
			];
		}
		return $this->output->set_output(json_encode($result));
	}

	public function hapus()
	{
		if (!$this->input->is_ajax_request()) {
			return;
		} 
		$data 			= $this->input->post('data_id'); 

		if ($this->m_adm_user->hapus($data)) { 

			$result = [
				'status' => true, 'pesan' => 'hapus berhasil'
			];

		}else{ 

			$result = [
				'status' => false, 'pesan' => 'gagal hapus'
			];
		}
		return $this->output->set_output(json_encode($result));
	}
}