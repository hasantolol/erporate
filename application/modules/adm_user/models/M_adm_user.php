<?php

require_once APPPATH . 'models/M_model_base.php';

class M_adm_user extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}

	public function get_user($data = null, $id_user=null)
	{
		$sql = "
		SELECT 
        * 
		from 
		user 
		WHERE (1=1)"; 
		$rowCount = $this->db->query($sql)->num_rows();
		$search   = $data['search']['value']; 
		$where    = '';

		if ($search != '') {
			$where .= "
			AND (nama LIKE '%" . $this->db->escape_like_str($search) . "%'";
			$where .= "
			OR username LIKE '%" . $this->db->escape_like_str($search) . "%'";
			$where .= "
			OR email LIKE '%" . $this->db->escape_like_str($search) . "%')";
		}  

		$sql       = $sql . $where .' ORDER BY nama ASC';
		// echo $sql;
		$filterRow = $this->db->query($sql)->num_rows();
		$start     = $data['start'];
		$length    = $data['length'];

		$sql .= " LIMIT {$start}, {$length}"; 

		$list = $this->db->query($sql);
		$option = [
			'draw'            => $data['draw'],
			'recordsTotal'    => $rowCount,
			'recordsFiltered' => $filterRow,
			'data'            => [],
		];

		$no = $this->input->post('start', true) + 1;

		foreach ($list->result() as $row) {
			$rows = [
				'no'            => $no . '.',
				'id_user'   	=> $row->id_user,
				'nama'			=> $row->nama,
				'username'		=> $row->username,  
				'email'			=> $row->email,  
				'aksi'    		=> '<buttton class="btn btn-primary edit btn-sm" data-id="'.$row->id_user.'">Ubah</buttton> <buttton class="btn btn-danger hapus btn-sm" data-id="'.$row->id_user.'">Hapus</buttton>',
			];
			$option['data'][] = $rows;
			$no++;
		}

		return $option;
	}
	public function tambah($data)
	{ 

		$this->db->trans_begin(); 

		$this->db->insert('user', $data);
		if ($this->db->trans_status() === false) {
			$this->db->trans_rollback(); 
			return false;
		} else {
			$this->db->trans_commit(); 
			return true;
		} 
	}

	public function edit($data)
	{  

		$this->db->trans_begin(); 

		$this->db->where('id_user', $data['id_user']);
		$this->db->update('user', $data); 
		// print_r($this->db->last_query()); 
		if ($this->db->trans_status() === false) {
			$this->db->trans_rollback(); 
			return false;
		} else {
			$this->db->trans_commit(); 
			return true;
		}
	}

	public function data_user($data)
	{ 
		$query = $this->db->get_where('user',array('id_user' => $data));
		if ($query->num_rows() >0) {
			return $query->row_array();
		}else{
			return [];
		}
	}

	public function hapus($data)
	{
		return $this->db->delete('user', array('id_user' => $data));
	}
}
