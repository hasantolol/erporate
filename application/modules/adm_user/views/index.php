
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-body">
        <div  class="container">

          <div class="row">
            <div class="col-md-8 col-sm-8 col-xs-12">
              <form class="form-inline" onkeypress="">
                <div class="form-group mx-sm-3">
                  <label for="inputPassword2" class="sr-only">Cari</label>
                  <input type="text" class="form-control" id="txt-cari" placeholder="Nama / Username">
                </div>  
              </form>
            </div>

            <div class="col-md-4 col-sm-4 col-xs-12">
              <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#exampleModal">
                Tambah User
              </button> 
            </div>
          </div>
          <br>
          <br>
          <table class="table" id="user">
            <thead class="thead-dark"> 
              <td scope="col" width="80px">No</td>
              <td scope="col">Nama</td> 
              <td scope="col">Username</td> 
              <td scope="col">Email</td> 
              <td scope="col">Aksi</td> 
            </thead>
            <tbody> 
            </tbody>
          </table>
        </div> 
      </div>
    </div>
  </div>
</div>

<!-- Modal Tambah -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form id="ftambah" onsubmit="return false">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Tambah Menu</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="form-group"> 
            <label for="nama">Nama</label>
            <input type="text" class="form-control" id="nama" aria-describedby="nama" placeholder="Nama User" name="nama" required="required"> 
          </div>
          <div class="form-group">              
            <label for="email">Email</label>
            <input type="email" class="form-control" id="email" aria-describedby="email" placeholder="email" name="email" required="required">  
          </div>
          <div class="form-group">
            <label for="ready">Hak Akses</label>
            <select name="hak_akses"  class="form-control">
              <option value="admin">Admin</option>
              <option value="kasir">Kasir</option>
              <option value="pelayan">Pelayan</option> 
            </select>
          </div>
          <div class="form-group">              
            <label for="username">Username</label>
            <input type="text" class="form-control" id="username" aria-describedby="username" placeholder="username" name="username" required="required">  
          </div> 
          <div class="form-group">              
            <label for="password">Password</label>
            <input type="password" class="form-control" id="password" aria-describedby="password" placeholder="password" name="password" required="required">  
          </div>
        </div>
        <div class="modal-footer"> 
          <button type="reset" class="btn btn-info" >Reset</button>
          <button type="submit" class="btn btn-primary" id="tombol-simpan">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form id="fedit" onsubmit="return false">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ubah Menu</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="form-group"> 
            <label for="nama">Nama</label>
            <input type="hidden" name="id_user" readonly="readonly">
            <input type="text" class="form-control" id="nama" aria-describedby="nama" placeholder="Nama Menu" name="nama" required="required"> 
          </div>
          <div class="form-group">              
            <label for="email">Email</label>
            <input type="email" class="form-control" id="email" aria-describedby="email" placeholder="email" name="email" required="required">  
          </div>
          <div class="form-group">
            <label for="ready">Hak Akses</label>
            <select name="hak_akses"  class="form-control">
              <option value="admin">Admin</option>
              <option value="kasir">Kasir</option>
              <option value="pelayan">Pelayan</option> 
            </select>
          </div>
          <div class="form-group">              
            <label for="username">Username</label>
            <input type="text" class="form-control" id="username" aria-describedby="username" placeholder="username" name="username" required="required">  
          </div> 
          <div class="form-group">              
            <label for="password">Password</label>
            <input type="password" class="form-control" id="password" aria-describedby="password" placeholder="password" name="password">  
          </div>
        </div>
        <div class="modal-footer"> 
          <button type="reset" class="btn btn-info" >Reset</button>
          <button type="submit" class="btn btn-primary" id="tombol-simpan">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>