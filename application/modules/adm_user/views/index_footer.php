<script>
    $(document).ready( function () { 
        duser = $("#user").DataTable({
            'ajax': {
                'url': "<?= base_url('adm_user/get_user') ?>",
                'timeout': 5000,
                'type': 'POST',
                "data":function(d){
                    d.search["value"]   = $("#txt-cari").val();
                    d.status            = $("#status").val();
                }
            }, 
            "serverSide": true,
            "bFilter": false,
            "pageLength": 25, 
            "ordering":true,
            "lengthChange": false,
            "columns": [
            {"data": "no", className : 'text-center', "orderable": "false"},
            {"data": "nama", "orderable": "false"}, 
            {"data": "username", "orderable": "false"}, 
            {"data": "email", "orderable": "false"}, 
            {"data": "aksi", className : 'text-center', "orderable": "false"}, 
            ]
        });
    } );
        // text-cari
        $("#txt-cari").on("keyup", function(){
            duser.ajax.reload(null, false);
        }) 

    // tombol ubah
    $('#user').on('click', '.edit', function () {
        /* var modal dan form */
        var modal = '#modal-edit';
        var form = '#fedit';
        var data_id = $(this).attr('data-id');
        $.ajax({
            url: "<?=base_url('adm_user/data_user')?>",
            type: "POST",
            data: "data_id=" + data_id,
            timeout: 5000,
            dataType: "JSON",
            success: function (data)
            { 
                if (data.status) 
                {
                    $(form + ' [name="id_user"]').val(data.data.id_user);
                    $(form + ' [name="nama"]').val(data.data.nama);
                    $(form + ' [name="email"]').val(data.data.email); 
                    $(form + ' [name="hak_akses"]').val(data.data.hak_akses); 
                    $(form + ' [name="username"]').val(data.data.username); 

                    $(modal).modal('show');
                } else {
                    alert(data.pesan);
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert(textStatus +jqXHR.status);
            }
        })
    });
        // this is the id of the form
        $("#ftambah").submit(function(e) {  
            let form = '#ftambah';
            let modal = '#exampleModal';
            let btn_submit_text = $(form + ' #tombol-simpan').text();
            $.ajax({
                url: "<?= base_url('adm_user/tambah') ?>",
                type: 'POST',
                data: $("#ftambah").serialize(),
                timeout: 10000,
                dataType: 'JSON',
                success: function (data) {
                    if (data.status) {
                        alert(data.pesan);  
                        $(modal).modal('hide');
                        $(form)[0].reset();
                        duser.ajax.reload(null, false);
                    } else {
                        alert(data.pesan); 
                        $(form + ' #tombol-simpan').text(btn_submit_text).attr('disabled', false);
                    }
                },
                error: function (textStatus) {
                    alert(textStatus);
                    $(form + ' #tombol-simpan').text(btn_submit_text).attr('disabled', false);
                }
            }); 


        }); 

        $("#fedit").submit(function(e) {  
            let form = '#fedit';
            let modal = '#modal-edit';
            let btn_submit_text = $(form + ' #tombol-simpan').text();
            $.ajax({
                url: "<?= base_url('adm_user/edit') ?>",
                type: 'POST',
                data: $("#fedit").serialize(),
                timeout: 10000,
                dataType: 'JSON',
                success: function (data) {
                    if (data.status) {
                        alert(data.pesan);  
                        $(modal).modal('hide');
                        $(form)[0].reset();
                        duser.ajax.reload(null, false);
                    } else {
                        alert(data.pesan); 
                        $(form + ' #tombol-simpan').text(btn_submit_text).attr('disabled', false);
                    }
                },
                error: function (textStatus) {
                    alert(textStatus);
                    $(form + ' #tombol-simpan').text(btn_submit_text).attr('disabled', false);
                }
            }); 


        }); 

        $('#user').on('click', '.hapus', function () {

            var data_id = $(this).attr('data-id'); 

            if (confirm("Apakah Anda yakin akan menghapus data ini?") == true) {
                $.ajax({
                    url: "<?=base_url('adm_user/hapus')?>",
                    type: "POST",
                    data: "data_id=" + data_id,
                    timeout: 5000,
                    dataType: "JSON",
                    success: function (data)
                    { 
                        if (data.status)  
                        {
                            alert(data.pesan);  
                            duser.ajax.reload(null, false);
                        } else {
                            alert(data.pesan);  
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert(textStatus);
                    }
                }) 
            }
        });
    </script>