
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-body">
        <div  class="container">

          <div class="row">
            <div class="col-md-8 col-sm-8 col-xs-12">
              <form class="form-inline" onkeypress="">
                <div class="form-group mx-sm-3">
                  <label for="inputPassword2" class="sr-only">Cari</label>
                  <input type="text" class="form-control" id="txt-cari" placeholder="No Pesanan / Meja">
                </div> 
                <div class="form-group mx-sm-4">
                  <select class="form-control select2" id="status">
                    <option value="">Semua Status</option>
                    <option value="1">Ready</option>
                    <option value="0">Non Ready</option> 
                  </select>
                </div>
              </form>
            </div>

            <div class="col-md-4 col-sm-4 col-xs-12">
              <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#exampleModal">
                Tambah Pesanan
              </button> 
            </div>
          </div>
          <br>
          <br>
          <table class="table" id="t_menu">
            <thead class="thead-dark"> 
              <td scope="col" width="80px">No</td>
              <td scope="col">Menu</td> 
              <td scope="col">Harga</td> 
              <td scope="col">Status</td> 
              <td scope="col">Aksi</td> 
            </thead>
            <tbody> 
            </tbody>
          </table>
        </div> 
      </div>
    </div>
  </div>
</div>

<!-- Modal Tambah -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form id="ftambah" onsubmit="return false">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Tambah Menu</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="form-group"> 
            <label for="nama_menu">Nama Menu</label>
            <input type="text" class="form-control" id="nama_menu" aria-describedby="nama_menu" placeholder="Nama Menu" name="nama_menu"> 
          </div>
          <div class="form-group">              <label for="harga">Harga</label>
            <input type="text" class="form-control" id="harga" aria-describedby="harga" placeholder="Harga" name="harga">  
          </div>
          <div class="form-group">
            <label for="ready">Status</label>
            <select name="ready"  class="form-control">
              <option value="1">Ready</option>
              <option value="0">Non Ready</option>
            </select>
          </div>
        </div>
        <div class="modal-footer"> 
          <button type="reset" class="btn btn-info" >Reset</button>
          <button type="submit" class="btn btn-primary" id="tombol-simpan">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>

  <!-- Modal -->
  <div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <form id="fedit" onsubmit="return false">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ubah Menu</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        <div class="modal-body">
          <div class="form-group"> 
            <label for="nama_menu">Nama Menu</label>
            <input type="hidden" name="id_menu" readonly>
            <input type="text" class="form-control" id="nama_menu" aria-describedby="nama_menu" placeholder="Nama Menu" name="nama_menu"> 
          </div>
          <div class="form-group">              <label for="harga">Harga</label>
            <input type="text" class="form-control" id="harga" aria-describedby="harga" placeholder="Harga" name="harga">  
          </div>
          <div class="form-group">
            <label for="ready">Status</label>
            <select name="ready"  class="form-control">
              <option value="1">Ready</option>
              <option value="0">Non Ready</option>
            </select>
          </div>
        </div>
          <div class="modal-footer"> 
            <button type="reset" class="btn btn-info" >Reset</button>
            <button type="submit" class="btn btn-primary" id="tombol-simpan">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>