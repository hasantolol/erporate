<script>
    $(document).ready( function () { 
        dt_menu = $("#t_menu").DataTable({
            'ajax': {
                'url': "<?= base_url('p_menu/get_menu') ?>",
                'timeout': 5000,
                'type': 'POST',
                "data":function(d){
                    d.search["value"]   = $("#txt-cari").val();
                    d.status            = $("#status").val();
                }
            }, 
            "serverSide": true,
            "bFilter": false,
            "pageLength": 25, 
            "ordering":true,
            "lengthChange": false,
            "columns": [
            {"data": "no", className : 'text-center', "orderable": "false"},
            {"data": "nama_menu", "orderable": "false"}, 
            {"data": "harga", "orderable": "false"}, 
            {"data": "ready", "orderable": "false"}, 
            {"data": "aksi", className : 'text-center', "orderable": "false"}, 
            ]
        });
    } );
        // text-cari
        $("#txt-cari").on("keyup", function(){
            dt_menu.ajax.reload(null, false);
        })

        // onchange select status dan sifat
        $(" #status").on("change", function(){
            dt_menu.ajax.reload(null, false);
        });  

    // tombol ubah
    $('#t_menu').on('click', '.edit', function () {
        /* var modal dan form */
        var modal = '#modal-edit';
        var form = '#fedit';
        var data_id = $(this).attr('data-id');
        $.ajax({
            url: "<?=base_url('p_menu/data_menu')?>",
            type: "POST",
            data: "data_id=" + data_id,
            timeout: 5000,
            dataType: "JSON",
            success: function (data)
            { 
                if (data.status) 
                {
                    $(form + ' [name="id_menu"]').val(data.data.id_menu);
                    $(form + ' [name="nama_menu"]').val(data.data.nama_menu);
                    $(form + ' [name="harga"]').val(data.data.harga); 
                    $(form + ' [name="ready"]').val(data.data.ready); 

                    $(modal).modal('show');
                } else {
                    alert(data.pesan);
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert(textStatus +jqXHR.status);
            }
        })
    });
        // this is the id of the form
        $("#ftambah").submit(function(e) {  
            let form = '#ftambah';
            let modal = '#exampleModal';
            let btn_submit_text = $(form + ' #tombol-simpan').text();
            $.ajax({
                url: "<?= base_url('p_menu/tambah') ?>",
                type: 'POST',
                data: $("#ftambah").serialize(),
                timeout: 10000,
                dataType: 'JSON',
                success: function (data) {
                    if (data.status) {
                        alert(data.pesan);  
                        $(modal).modal('hide');
                        $(form)[0].reset();
                        dt_menu.ajax.reload(null, false);
                    } else {
                        alert(data.pesan); 
                        $(form + ' #tombol-simpan').text(btn_submit_text).attr('disabled', false);
                    }
                },
                error: function (textStatus) {
                    alert(textStatus);
                    $(form + ' #tombol-simpan').text(btn_submit_text).attr('disabled', false);
                }
            }); 


        }); 

        $("#fedit").submit(function(e) {  
            let form = '#fedit';
            let modal = '#modal-edit';
            let btn_submit_text = $(form + ' #tombol-simpan').text();
            $.ajax({
                url: "<?= base_url('p_menu/edit') ?>",
                type: 'POST',
                data: $("#fedit").serialize(),
                timeout: 10000,
                dataType: 'JSON',
                success: function (data) {
                    if (data.status) {
                        alert(data.pesan);  
                        $(modal).modal('hide');
                        $(form)[0].reset();
                        dt_menu.ajax.reload(null, false);
                    } else {
                        alert(data.pesan); 
                        $(form + ' #tombol-simpan').text(btn_submit_text).attr('disabled', false);
                    }
                },
                error: function (textStatus) {
                    alert(textStatus);
                    $(form + ' #tombol-simpan').text(btn_submit_text).attr('disabled', false);
                }
            }); 


        }); 

        $('#t_menu').on('click', '.hapus', function () {

            var data_id = $(this).attr('data-id'); 

            if (confirm("Apakah Anda yakin akan menghapus data ini?") == true) {
                $.ajax({
                    url: "<?=base_url('p_menu/hapus')?>",
                    type: "POST",
                    data: "data_id=" + data_id,
                    timeout: 5000,
                    dataType: "JSON",
                    success: function (data)
                    { 
                        if (data.status)  
                        {
                            alert(data.pesan);  
                            dt_menu.ajax.reload(null, false);
                        } else {
                            alert(data.pesan);  
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert(textStatus);
                    }
                }) 
            }
        });
    </script>