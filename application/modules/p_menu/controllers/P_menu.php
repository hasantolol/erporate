<?php
require_once APPPATH . 'controllers/Core_base.php';

class P_menu extends Core_base {

	public function __construct() {
		$this->load->model('m_p_menu');
        // Check that the user is logged in
		// if (($this->session->userdata('id_user') == null || $this->session->userdata('id_user') < 1) AND $this->session->userdata('hak_akses') == 'pelayan') {
		if ($this->session->userdata('status') != 'login' AND $this->session->userdata('hak_akses') != 'pelayan') {
            // Prevent infinite loop by checking that this isn't the login controller               
			if ( $this->router->fetch_class() != 'auth') 
			{                        
				redirect(base_url());
			}
		}
		parent::__construct(); 
	}

	public function index() { 
		parent::display('index',NULL,'index_footer'); 
	}
	public function get_menu()
	{ 
		// echo "<pre>";
		// print_r($this->input->post());
		if (!$this->input->is_ajax_request()) {
			return;
		}  
		$datatable = $this->m_p_menu->get_menu($this->input->post(), $this->session->userdata('id_user'));

		$this->output->set_output(json_encode($datatable));
	} 
	
	public function tambah()
	{
		if (!$this->input->is_ajax_request()) {
			return;
		} 
		$data['id_menu'] 		= $this->uuid->v4(true);
		$data['nama_menu'] 		= $this->input->post('nama_menu');
		$data['harga'] 			= $this->input->post('harga');
		$data['ready'] 			= $this->input->post('ready');
		$data['created_by']		= $this->session->userdata('id_user');
		$data['created_date']		= date('Y-m-d H:i:s');

		if ($this->m_p_menu->tambah($data)) { 

			$result = [
				'status' => true, 'pesan' => 'tambah berhasil'
			];

		}else{

			$result = [
				'status' => false, 'pesan' => 'gagal tambah'
			];
		}
		return $this->output->set_output(json_encode($result));
	}

	public function data_menu()
	{ 

        // validasi hanya request lewat ajax
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
        // get data
		$data['data'] = $this->m_p_menu->data_menu($this->input->post('data_id')); 

        // validasi jika kosong
		if (empty($data['data'])) {
			return $this->output->set_output(json_encode(array('pesan' => 'data tidak ditemukan!', 'data' => null, 'status' => 0)));
		}
		$data['status'] = true;
		return $this->output->set_output(json_encode($data));

	}

	public function edit()
	{
		if (!$this->input->is_ajax_request()) {
			return;
		} 
		$data['id_menu'] 		= $this->input->post('id_menu');
		$data['nama_menu'] 		= $this->input->post('nama_menu');
		$data['harga'] 			= $this->input->post('harga');
		$data['ready'] 			= $this->input->post('ready');
		$data['created_by']		= $this->session->userdata('id_user');
		$data['created_date']		= date('Y-m-d H:i:s');

		if ($this->m_p_menu->edit($data)) { 

			$result = [
				'status' => true, 'pesan' => 'ubah berhasil'
			];

		}else{

			$result = [
				'status' => false, 'pesan' => 'gagal ubah'
			];
		}
		return $this->output->set_output(json_encode($result));
	}

	public function hapus()
	{
		if (!$this->input->is_ajax_request()) {
			return;
		} 
		$data 			= $this->input->post('data_id'); 

		if ($this->m_p_menu->hapus($data)) { 

			$result = [
				'status' => true, 'pesan' => 'hapus berhasil'
			];

		}else{ 

			$result = [
				'status' => false, 'pesan' => 'gagal hapus'
			];
		}
		return $this->output->set_output(json_encode($result));
	}
}