<?php

require_once APPPATH . 'models/M_model_base.php';

class M_p_menu extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}

	public function get_menu($data = null, $id_user=null)
	{
		$sql = "
		SELECT 
        * 
		from 
		menu "; 
		$rowCount = $this->db->query($sql)->num_rows();
		$search   = $data['search']['value'];
		$status   = $data['status'];
		$where    = '';

		if ($search != '' OR $status != '') {
			$where .= "
			WHERE (1=1)";
		} 
		if ($search != '') {
			$where .= "
			AND (nama_menu LIKE '%" . $this->db->escape_like_str($search) . "%')";
		} 
		if ($status != '') {
			$where .= "
			AND (ready ='" . $status. "')
			";
		}

		$sql       = $sql . $where .' ORDER BY nama_menu ASC';
		// echo $sql;
		$filterRow = $this->db->query($sql)->num_rows();
		$start     = $data['start'];
		$length    = $data['length'];

		$sql .= " LIMIT {$start}, {$length}"; 

		$list = $this->db->query($sql);
		$option = [
			'draw'            => $data['draw'],
			'recordsTotal'    => $rowCount,
			'recordsFiltered' => $filterRow,
			'data'            => [],
		];

		$no = $this->input->post('start', true) + 1;

		foreach ($list->result() as $row) {
			$rows = [
				'no'            => $no . '.',
				'id_menu'   	=> $row->id_menu,
				'nama_menu'		=> $row->nama_menu,
				'harga'			=> $row->harga,
				'ready'    		=> ($row->ready)?'Ready':'Not Ready',  
				'aksi'    		=> '<buttton class="btn btn-primary edit btn-sm" data-id="'.$row->id_menu.'">Ubah</buttton> <buttton class="btn btn-danger hapus btn-sm" data-id="'.$row->id_menu.'">Hapus</buttton>',
			];
			$option['data'][] = $rows;
			$no++;
		}

		return $option;
	}
	public function tambah($data)
	{ 

		$this->db->trans_begin(); 

		$this->db->insert('menu', $data);
		if ($this->db->trans_status() === false) {
			$this->db->trans_rollback(); 
			return false;
		} else {
			$this->db->trans_commit(); 
			return true;
		} 
	}

	public function edit($data)
	{  

		$this->db->trans_begin(); 

		$this->db->where('id_menu', $data['id_menu']);
		$this->db->update('menu', $data); 
		if ($this->db->trans_status() === false) {
			$this->db->trans_rollback(); 
			return false;
		} else {
			$this->db->trans_commit(); 
			return true;
		}
	}

	public function data_menu($data)
	{ 
		$query = $this->db->get_where('menu',array('id_menu' => $data));
		if ($query->num_rows() >0) {
			return $query->row_array();
		}else{
			return [];
		}
	}

	public function hapus($data)
	{
		return $this->db->delete('menu', array('id_menu' => $data));
	}
}
