<?php
require_once APPPATH . 'controllers/Core_base.php';

class Adm_akun extends Core_base {

	public function __construct() {
		parent::__construct(); 
		$this->load->model('m_adm_akun');
	}

	public function index() {  
		parent::display('index',NULL,'index_footer'); 
	}

	public function get_datatable(){
		$datatable = $this->m_adm_akun->get_datatable($this->input->post());

		$this->output->set_output(json_encode($datatable));
	}
}
