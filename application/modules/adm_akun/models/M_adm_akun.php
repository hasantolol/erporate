<?php

require_once APPPATH . 'models/M_model_base.php';

class M_adm_akun extends M_model_base {

	public function __construct() {
		parent::__construct();
	}


	public function get_datatable($data = null)
	{
		$sql = "
		SELECT *
		FROM user
		WHERE( hak_akses !='admin')
		"; 
		$rowCount = $this->db->query($sql)->num_rows();
		$search   = $data['search']['value'];
		$where    = '';

		if ($search != '') {
			$where .= "
			AND (nama LIKE '%" . $this->db->escape_like_str($search) . "%'
			";
			$where .= "
			OR username LIKE '%" . $this->db->escape_like_str($search) . "%'
			";
			$where .= "
			OR email LIKE '%" . $this->db->escape_like_str($search) . "%')
			"; 
		}

		$sql       = $sql . $where .'ORDER BY nama ASC';
		$filterRow = $this->db->query($sql)->num_rows();
		$start     = $data['start'];
		$length    = $data['length'];

		$sql .= " LIMIT {$start}, {$length}";

		$list = $this->db->query($sql);

		$option = [
			'draw'            => $data['draw'],
			'recordsTotal'    => $rowCount,
			'recordsFiltered' => $filterRow,
			'data'            => [],
		];

		$no = $this->input->post('start', true) + 1;

		foreach ($list->result() as $row) {
			$rows = [
				'no'            => $no . '.',
				'id'            => $row->id,
				'nama'    		=> $row->nama,
				'username'    	=> $row->username, 
				'email'    		=> $row->email, 
				'aksi'    		=> '<div class="text-center"> 
				<button type="button" class="btn btn-info"><i class=" fas fa-eye"></i></button>
				<button type="button" class="btn btn-primary"><i class="fas fa-pencil-alt"></i></button>
				<button type="button" class="btn btn-danger"><i class="fas fa-trash-alt"></i></button>
				</div>', 
			];
			$option['data'][] = $rows;
			$no++;
		}

		return $option;
	}
}
