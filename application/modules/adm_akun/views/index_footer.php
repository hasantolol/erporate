
<script>
    $(document).ready(function() { 
        dt_akun = $("#t_akun").DataTable({
            'ajax': {
                'url':  "<?= base_url('adm_akun/get_datatable') ?>",
                'type': 'POST'
            },
            "processing": false,
            "serverSide": true,
            "pageLength": 25,
            "paging": true,
            'columns': [{
                data: 'no',
                className: ' text-center',
                searchable: false,
                orderable:false
            },
            {
                data: 'nama', 
            },
            {
                data: 'email', 
            },
            {
                data: 'username', 
            },
            {
                data: 'aksi', 
            }
            ]
        });
    });

</script>