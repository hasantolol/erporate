<?php
require_once APPPATH . 'controllers/Core_base.php';

defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends Core_base {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_auth');
        // Check that the user is logged in
		if ($this->session->userdata('status') != 'login') {
            // Prevent infinite loop by checking that this isn't the login controller               
			if ( $this->router->fetch_class() != 'auth') 
			{                        
				redirect(base_url());
			}
		} 
	}

	public function index()
	{   
		if ($this->session->userdata('status') == 'login')  {
			redirect(base_url('dashboard'));
		} 
		$this->load->view('index');
	}

	function do_login(){ 

		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$where = array(
			'username' => $username,
			'password' => md5($password)
		);

		$cek = $this->M_auth->cek_login("user",$where);   
		if(count($cek) > 0){

			$data_session = array(
				'id_user' => $cek['id_user'],
				'username' => $cek['username'],
				'hak_akses' => $cek['hak_akses'],
				'status' => "login"
			);

			$this->session->set_userdata($data_session);

			$result = [
				'status' => true, 'pesan' => 'login berhasil','redirect'=> base_url('dashboard')
			];

		}else{

			$result = [
				'status' => false, 'pesan' => 'gagal login'
			];
		}
		return $this->output->set_output(json_encode($result));
	}

	function logout(){
		$this->session->sess_destroy(); 
		redirect(base_url());
	}
}