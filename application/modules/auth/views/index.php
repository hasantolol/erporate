<!DOCTYPE html>
<html>
<head>
  <title></title>
  <!-- Bootstrap Core CSS -->
  <link href="<?= base_url() ?>assets/layout/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?= base_url() ?>assets/layout/plugins/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet"> 
  <link href="<?= base_url() ?>assets/layout/css/login.css" rel="stylesheet"> 
</head>
<body>
  <div class="wrapper fadeInDown">
    <div id="formContent" style="width: 300px;">
      <!-- Tabs Titles -->

      <!-- Icon -->
      <div class="fadeIn first">
        <img src="<?= base_url() ?>assets/layout/images/login.jpg" id="icon" alt="User Icon" />
      </div>

      <!-- Login Form -->
      <form id="flogin" onsubmit="return false">
        <input type="text" id="username" class="fadeIn second" name="username" placeholder="username" required="required">
        <input type="password" id="password" class="fadeIn third" name="password" placeholder="password" required="required"> 
        <input type="submit" class="fadeIn fourth" value="Log In">
      </form>

      <!-- Remind Passowrd -->
      <div id="formFooter">
        <a class="underlineHover" href="#">Forgot Password?</a>
      </div>

    </div>
  </div>

  <script src="<?= base_url() ?>assets/layout/plugins/jquery/jquery.min.js"></script>
  <script type="text/javascript">
    // this is the id of the form
    $("#flogin").submit(function(e) {  
      let form = '#flogin';
      let btn_submit_text = $(form + ' #tombol-simpan').text();
      $.ajax({
        url: "<?= base_url('auth/do_login') ?>",
        type: 'POST',
        data: $("#flogin").serialize(),
        timeout: 10000,
        dataType: 'JSON',
        success: function (data) {
          if (data.status) {

            alert(data.pesan); 
            window.location.href =data.redirect;
          } else {
            alert(data.pesan); 
            $(form + ' #tombol-simpan').text(btn_submit_text).attr('disabled', false);
          }
        },
        error: function (textStatus) {
          alert(textStatus);
          $(form + ' #tombol-simpan').text(btn_submit_text).attr('disabled', false);
        }
      }); 


    });
  </script>
</body>
</html>