<?php

require_once APPPATH . 'models/M_model_base.php';

class M_auth extends M_model_base {


	public function __construct()
	{
		parent::__construct();
	}

	public function cek_login($table, $where)
	{
		$rows = $this->db->get_where($table, $where);
		
		if ($rows->num_rows() > 0) { 
			return $rows->row_array();
		}else{
			return [];
		}

	}
}
