<?php

require_once APPPATH . 'models/M_model_base.php';

class M_dashboard extends M_model_base {

    public function __construct() {
        parent::__construct();
    }

   public function countRow($params = NULL){
     $sql = "SELECT * FROM ".$params;
     $query = $this->db->query($sql);
     $result = $query->num_rows();
     return $result;
   }
}
