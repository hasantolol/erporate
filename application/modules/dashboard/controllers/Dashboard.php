<?php
require_once APPPATH . 'controllers/Core_base.php';

class Dashboard extends Core_base {

	public function __construct() {
        // Check that the user is logged in
		// if (($this->session->userdata('id_user') == null || $this->session->userdata('id_user') < 1) AND $this->session->userdata('hak_akses') == 'pelayan') {
		if ($this->session->userdata('status') != 'login') {
            // Prevent infinite loop by checking that this isn't the login controller               
			if ( $this->router->fetch_class() != 'auth') 
			{                        
				redirect(base_url());
			}
		}
        parent::__construct(); 
    }

    public function index() { 
        parent::display('index',NULL,'index_footer'); 
    }

}
