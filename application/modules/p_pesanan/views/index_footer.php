<script>
    $(document).ready( function () { 
        dpesanan = $("#pesanan").DataTable({
            'ajax': {
                'url': "<?= base_url('p_pesanan/get_pesanan') ?>",
                'timeout': 5000,
                'type': 'POST',
                "data":function(d){
                    d.search["value"]   = $("#txt-cari").val();
                    d.status            = $("#status").val();
                }
            }, 
            "serverSide": true,
            "bFilter": false,
            "pageLength": 25, 
            "ordering":true,
            "lengthChange": false,
            "columns": [
            {"data": "no", className : 'text-center', "orderable": "false"},
            {"data": "nomor_pesanan", "orderable": "false"}, 
            {"data": "nomor_meja", "orderable": "false"}, 
            {"data": "status", "orderable": "false"}, 
            {"data": "aksi", className : 'text-center', "orderable": "false"}, 
            ]
        });
    } );
        // text-cari
        $("#txt-cari").on("keyup", function(){
            dpesanan.ajax.reload(null, false);
        })

        // onchange select status dan sifat
        $(" #status").on("change", function(){
            dpesanan.ajax.reload(null, false);
        });  

    // tombol ubah
    $('#pesanan').on('click', '.edit', function () {
        /* var modal dan form */
        var modal = '#modal-edit';
        var form = '#fedit';
        var data_id = $(this).attr('data-id');
        $.ajax({
            url: "<?=base_url('p_pesanan/data_pesanan')?>",
            type: "POST",
            data: "data_id=" + data_id,
            timeout: 5000,
            dataType: "JSON",
            success: function (data)
            { 
                if (data.status) 
                {
                    $(form + ' [name="id_pesanan"]').val(data.data.id_pesanan);
                    $(form + ' [name="nomor_pesanan"]').val(data.data.nomor_pesanan); 
                    $(form + ' [name="nomor_meja"]').val(data.data.nomor_meja); 

                    $('fedit-tmenu').empty();
                    $.each(data.rs_data, function(index, val) { 
                        addrow('fedit-tmenu',val);
                    });

                    if (data.data.status == 1) {
                        $('#status-pembayaran').empty();
                        $('#label-status-pembayaran').empty();
                        $('#status-pembayaran').append('<span class="label label-success">Selesai</span>');
                        $('#label-status-pembayaran').append('<table style="width:100%;"><tr><td>Status Telah Dibayar dan Di proses oleh</td><td>'+data.pembayaran.nama+'</td></tr><tr><td>Waktu</td><td>'+data.pembayaran.tanggal_pembayaran+'</td></tr></table>');
                    }else if(data.data.status == 0){
                        $('#status-pembayaran').empty();
                        $('#label-status-pembayaran').empty();
                        $('#status-pembayaran').append('<span class="label label-danger">Belum Dibayar</span>');
                    }

                    $(modal).modal('show');
                } else {
                    alert(data.pesan);
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert(textStatus +jqXHR.status);
            }
        })
    });
        // this is the id of the form
        $("#ftambah").submit(function(e) {  
            let form = '#ftambah';
            let modal = '#exampleModal';
            let btn_submit_text = $(form + ' #tombol-simpan').text();
            $.ajax({
                url: "<?= base_url('p_pesanan/tambah') ?>",
                type: 'POST',
                data: $("#ftambah").serialize(),
                timeout: 10000,
                dataType: 'JSON',
                success: function (data) {
                    if (data.status) {
                        alert(data.pesan);  
                        $(modal).modal('hide');
                        $(form)[0].reset();
                        dpesanan.ajax.reload(null, false);
                    } else {
                        alert(data.pesan); 
                        $(form + ' #tombol-simpan').text(btn_submit_text).attr('disabled', false);
                    }
                },
                error: function (textStatus) {
                    alert(textStatus);
                    $(form + ' #tombol-simpan').text(btn_submit_text).attr('disabled', false);
                }
            }); 


        }); 

        $("#fedit").submit(function(e) {  
            let form = '#fedit';
            let modal = '#modal-edit';
            let btn_submit_text = $(form + ' #tombol-simpan').text();
            $.ajax({
                url: "<?= base_url('p_pesanan/edit') ?>",
                type: 'POST',
                data: $("#fedit").serialize(),
                timeout: 10000,
                dataType: 'JSON',
                success: function (data) {
                    if (data.status) {
                        alert(data.pesan);  
                        $(modal).modal('hide');
                        $(form)[0].reset();
                        dpesanan.ajax.reload(null, false);
                    } else {
                        alert(data.pesan); 
                        $(form + ' #tombol-simpan').text(btn_submit_text).attr('disabled', false);
                    }
                },
                error: function (textStatus) {
                    alert(textStatus);
                    $(form + ' #tombol-simpan').text(btn_submit_text).attr('disabled', false);
                }
            }); 


        }); 

        $('#pesanan').on('click', '.hapus', function () {

            var data_id = $(this).attr('data-id'); 

            if (confirm("Apakah Anda yakin akan menghapus data ini?") == true) {
                $.ajax({
                    url: "<?=base_url('p_pesanan/hapus')?>",
                    type: "POST",
                    data: "data_id=" + data_id,
                    timeout: 5000,
                    dataType: "JSON",
                    success: function (data)
                    { 
                        if (data.status)  
                        {
                            alert(data.pesan);  
                            dpesanan.ajax.reload(null, false);
                        } else {
                            alert(data.pesan);  
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert(textStatus);
                    }
                }) 
            }
        });
        $( ".tambah-menu" ).click(function() {  
            addrow($(this).parent().find('table').attr('id'));
        }); 

        $('#exampleModal').on('hidden.bs.modal', function(e) {
            $(this).find('#ftambah')[0].reset();
            let table_id = $(this).find('#ftambah').find('table').attr('id'); 
            $('#'+table_id+' tbody ').empty();
            $(this).find('#ftambah #txt_total').empty();
        });
        $('#modal-edit').on('hidden.bs.modal', function(e) {
            $(this).find('#fedit')[0].reset();
            let table_id = $(this).find('#fedit').find('table').attr('id'); 
            $('#'+table_id+' tbody ').empty();
            $(this).find('#fedit #txt_total').empty();
        });

        function set_select(){

            $('.id-menu').select2({  
                width: "100%",   
                placeholder: "-- Pilih Menu --", 
                ajax :{
                    url : "<?php echo base_url('p_pesanan/get_select2_menu/'); ?>",
                    dataType: 'json',
                    type: 'post',
                    delay: 250,
                    data: function(params) {
                        return {
                            q: params.term || '', 
                            page_limit: 10,
                            page: params.page || 1
                        };
                    },
                    processResults: function(data, params) {
                        return {
                            results: data.items,
                            'pagination': {
                                'more': data.more
                            }
                        };
                    },
                    cache: true,
                }

            }); 
        };

        function set_harga(ths){
            let data = $(ths).select2('data')[0];
            $(ths).parent().parent().find('.harga').val(data.harga);
            count_table($(ths).closest('form').attr('id'));
            // console.log(data.harga);
        }
        function count_table(ths){ 
            let total = parseFloat(0);
            $('#'+ths+' .harga').each(function() { 
                harga = parseFloat( $( this ).val()); 
                jumlah = parseFloat($( this ).closest('tr').find('.jumlah').val()); 
                total += (harga *jumlah);
            });  
            $('#'+ths+' #txt_total').html(total);
            // console.log(total);
        }

        function deleteRow(ths, table_id){
            $(ths).closest('tr').remove(); 
            count_table(table_id);
        }

        function getObjectValue(obj, selector) {
            if (obj === null) {
                return '';
            }
            return obj[selector];
        }

        function addrow(table, val = null){
            // console.log(getObjectValue(val, 'id_menu'));
            let table_id = '#'+table;
            row = '<tr>';
            row += '<td ><select name="id_menu[]" class="form-control id-menu" onchange="set_harga(this)"><option value="' +getObjectValue(val, 'id_menu')+ ' selected ="selected">' +getObjectValue(val, 'nama_menu') + '</option></select></td>'; 
            row += '<td><input type="number" name="harga[]" class="form-control harga" value="' +getObjectValue(val, 'harga') + '" readonly></td>'; 
            row += '<td><input type="number" name="jumlah[]" class="form-control jumlah" value="1" onkeyup="count_table(\''+table+'\')"></td>'; 
            row += '<td class="text-center"><button type="button" class="btn btn-circle btn-icon-only btn-danger delRow"  onclick="deleteRow(this, \''+table+'\')"><i class="icon-trash"></i></button></td>'; 
            row += '<tr>';
            $(table_id).append(row); 
            set_select();
            count_table(table);
        }
    </script>