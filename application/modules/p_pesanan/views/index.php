
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-body">
        <div  class="container">

          <div class="row">
            <div class="col-md-8 col-sm-8 col-xs-12">
              <form class="form-inline" onkeypress="">
                <div class="form-group mx-sm-3">
                  <label for="inputPassword2" class="sr-only">Cari</label>
                  <input type="text" class="form-control" id="txt-cari" placeholder="No Pesanan / Meja">
                </div> 
                <div class="form-group mx-sm-4">
                  <select class="form-control select2" id="status">
                    <option value="">Semua Status</option>
                    <option value="1">Selesai</option>
                    <option value="0">Belum Dibayar</option> 
                  </select>
                </div>
              </form>
            </div>

            <div class="col-md-4 col-sm-4 col-xs-12">
              <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#exampleModal">
                Tambah Pesanan
              </button> 
            </div>
          </div>
          <table class="table" id="pesanan">
            <thead class="thead-dark"> 
              <td scope="col" width="80px">No</td>
              <td scope="col">Nomor Pesanan</td> 
              <td scope="col">Meja</td> 
              <td scope="col">Status</td>
              <td scope="col">Aksi</td> 
            </thead>
            <tbody> 
            </tbody>
          </table>
        </div> 
      </div>
    </div>
  </div>
</div>


<!-- Modal Tambah -->
<div class="modal fade" id="exampleModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <form id="ftambah" onsubmit="return false">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Tambah Pesanan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="form-group row">
            <div class="col-sm-4">
              <label for="nomor_meja">Meja</label>
              <input type="text" class="form-control" id="nomor_meja" aria-describedby="nomor_meja" placeholder="Nomor Meja" name="nomor_meja"> 
            </div>
            <div class="col-sm-8">
              <label for="nomor_pesanan">Nomor Pesanan</label>
              <input type="text" class="form-control" id="nomor_pesanan" disabled="disabled" aria-describedby="nomor_pesanan" placeholder="[AUTO]" name="nomor_pesanan"> 
            </div>
          </div>
          <div class="form-group">
            <label for="deskripsi">Menu</label>
            <table class="table" id="ftambah-tmenu">
              <thead>
                <td class="text-center">Nama Menu</td>
                <td class="text-center">Harga</td>
                <td class="text-center">Jumlah</td>
                <td class="text-center">Aksi</td>
              </thead>
              <tbody>
                
              </tbody>
              <tfoot>
                <td>Total</td>
                <td><span id="txt_total"></span> </td>
              </tfoot>
            </table>
            <button class="btn btn-primary tambah-menu" type="button">Tambah Pesanan</button>
          </div>
        </div>
        <div class="modal-footer"> 
          <button type="submit" class="btn btn-primary" id="tombol-simpan">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>


<!-- Modal Tambah -->
<div class="modal fade" id="modal-edit" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <form id="fedit" onsubmit="return false">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Tambah Pesanan</h5>
          <small id="status-pembayaran"></small>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="form-group row">
            <div class="col-sm-4">
              <label for="nomor_meja">Meja</label>
              <input type="hidden" name="id_pesanan">
              <input type="text" class="form-control" id="nomor_meja" aria-describedby="nomor_meja" placeholder="Nomor Meja" name="nomor_meja"> 
            </div>
            <div class="col-sm-8">
              <label for="nomor_pesanan">Nomor Pesanan</label>
              <input type="text" class="form-control" id="nomor_pesanan" disabled="disabled" aria-describedby="nomor_pesanan" placeholder="[AUTO]" name="nomor_pesanan"> 
            </div>
          </div>
          <div class="form-group">
            <label for="deskripsi">Menu</label>
            <table class="table" id="fedit-tmenu">
              <thead>
                <td class="text-center">Nama Menu</td>
                <td class="text-center">Harga</td>
                <td class="text-center">Jumlah</td>
                <td class="text-center">Aksi</td>
              </thead>
              <tbody>
                
              </tbody>
              <tfoot>
                <td>Total</td>
                <td><span id="txt_total"></span> </td>
              </tfoot>
            </table>
            <button class="btn btn-primary tambah-menu" type="button">Tambah Pesanan</button>
          </div>
          <div class="form-group">
            <div id="label-status-pembayaran">
              
            </div>
          </div>
        </div>
        <div class="modal-footer"> 
          <button type="submit" class="btn btn-primary" id="tombol-simpan">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>