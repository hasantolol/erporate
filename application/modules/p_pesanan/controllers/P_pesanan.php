<?php
require_once APPPATH . 'controllers/Core_base.php';

class P_pesanan extends Core_base {

	public function __construct() {
		$this->load->model('m_p_pesanan');
        // Check that the user is logged in
		// if (($this->session->userdata('id_user') == null || $this->session->userdata('id_user') < 1) AND $this->session->userdata('hak_akses') == 'pelayan') {
		if ($this->session->userdata('status') != 'login' AND $this->session->userdata('hak_akses') != 'pelayan') {
            // Prevent infinite loop by checking that this isn't the login controller               
			if ( $this->router->fetch_class() != 'auth') 
			{                        
				redirect(base_url());
			}
		}
		parent::__construct(); 
	}

	public function index() { 
		parent::display('index',NULL,'index_footer'); 
	}
	public function get_pesanan()
	{ 
		if (!$this->input->is_ajax_request()) {
			return;
		}  
		$datatable = $this->m_p_pesanan->get_pesanan($this->input->post(), $this->session->userdata('id_user'));

		$this->output->set_output(json_encode($datatable));
	} 
	
    // select2
	public function get_select2_menu()
	{
        // validasi request hanya dari ajax
		if (!$this->input->is_ajax_request()) {
			return;
		}
		$data = $this->m_p_pesanan->get_select2_menu();
		$this->output->set_output(json_encode($data));
        // echo "<pre>";
        // print_r($data);
	}

	public function tambah()
	{
		if (!$this->input->is_ajax_request()) {
			return;
		} 
		$data['id_pesanan']		= $this->uuid->v4(true);
		$data['nomor_pesanan']	= $this->get_nomor_pesanan();
		$data['nomor_meja']		= $this->input->post('nomor_meja');
		$data['status']			= '0';
		$data['created_by']		= $this->session->userdata('id_user');
		$data['created_date']	= date('Y-m-d H:i:s');

		$rs					=$this->get_rs($data['id_pesanan']);
		// echo "<pre>";
		// print_r($rs);
		// exit;

		if ($this->m_p_pesanan->tambah($data, $rs)) { 

			$result = [
				'status' => true, 'pesan' => 'tambah berhasil'
			];

		}else{
			// echo "<pre>";
			// print_r($result);
			$result = [
				'status' => false, 'pesan' => 'gagal tambah'
			];
		}
		return $this->output->set_output(json_encode($result));
	}

	public function get_rs($id_pesanan)
	{ 
		$rs_data = [];

		if (!empty($this->input->post('id_menu[]'))) {
			foreach ($this->input->post('id_menu[]') as $key => $value) {
				if (!empty($value)) {
					$rs_data[$key]['id_pesanan']      = $id_pesanan; 
					$rs_data[$key]['id_menu'] 		= $value;
				}
			}
		}

		if (!empty($this->input->post('jumlah[]'))) {
			foreach ($this->input->post('jumlah[]') as $key => $value) {
				if (!empty($value)) { 
					$rs_data[$key]['jumlah'] 		= $value;
				}
			}
		}

		return $rs_data;
	}

	public function get_nomor_pesanan(){ 
		$first 			= 'ERP'.date('dmy'); 
		$data_pesanan 	= $this->m_p_pesanan->get_last_pesanan();
		if (empty($data_pesanan)) {
			$number = 0;
			$number +=1;
			$nomor 	= str_pad($number, 3, "0", STR_PAD_LEFT);
		}else{
			$number 	= substr($data_pesanan['nomor_pesanan'], -3);
			$number 	+= 1;
			$nomor 		= str_pad($number, 3, "0", STR_PAD_LEFT);
		}
		return $first.'-'.$nomor;
	}

	public function data_pesanan()
	{ 

        // validasi hanya request lewat ajax
		if (!$this->input->is_ajax_request()) {
			show_404();
		}
        // get data
		$data['data'] = $this->m_p_pesanan->data_pesanan($this->input->post('data_id')); 
		$data['rs_data'] = $this->m_p_pesanan->data_menu_pesanan($this->input->post('data_id')); 
		$data['pembayaran'] = $this->m_p_pesanan->data_pembayaran($this->input->post('data_id'));
		// echo "<pre>";
		// print_r($data);
		// print_r($this->input->post('data_id'));
		// exit;

        // validasi jika kosong
		if (empty($data['data'])) {
			return $this->output->set_output(json_encode(array('pesan' => 'data tidak ditemukan!', 'data' => null, 'status' => 0)));
		}
		$data['status'] = true;
		return $this->output->set_output(json_encode($data));

	}


	public function edit()
	{
		if (!$this->input->is_ajax_request()) {
			return;
		} 
		$data['id_pesanan']		= $this->input->post('id_pesanan'); 
		$data['nomor_meja']		= $this->input->post('nomor_meja');
		$data['status']			= '0'; 

		$rs					=$this->get_rs($data['id_pesanan']);

		if ($this->m_p_pesanan->edit($data, $rs)) { 

			$result = [
				'status' => true, 'pesan' => 'ubah berhasil'
			];

		}else{

			$result = [
				'status' => false, 'pesan' => 'gagal ubah'
			];
		}
		return $this->output->set_output(json_encode($result));
	}

	public function hapus()
	{
		if (!$this->input->is_ajax_request()) {
			return;
		} 
		$data 			= $this->input->post('data_id'); 

		if ($this->m_p_pesanan->hapus($data)) { 

			$result = [
				'status' => true, 'pesan' => 'hapus berhasil'
			];

		}else{ 

			$result = [
				'status' => false, 'pesan' => 'gagal hapus'
			];
		}
		return $this->output->set_output(json_encode($result));
	}
}