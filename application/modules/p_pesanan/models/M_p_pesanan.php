<?php

require_once APPPATH . 'models/M_model_base.php';

class M_p_pesanan extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}

	public function get_pesanan($data = null, $id_user=null)
	{
		$sql = "
		SELECT 
        * 
		from 
		pesanan 
		WHERE created_by ='".$id_user."'"; 
		$rowCount = $this->db->query($sql)->num_rows();
		$search   = $data['search']['value'];
		$status   = $data['status'];
		$where    = '';

		if ($search != '') {
			$where .= "
			AND (nomor_pesanan LIKE '%" . $this->db->escape_like_str($search) . "%'
			";
		}
		if ($search != '') {
			$where .= "
			OR nomor_meja ='" . $this->db->escape_like_str($search) . "')
			";
		}
		if ($status != '') {
			$where .= "
			AND (status ='" . $this->db->escape_like_str($status) . "')
			";
		}

		$sql       = $sql . $where .' ORDER BY status ASC, created_date DESC, nomor_pesanan ASC';
		$filterRow = $this->db->query($sql)->num_rows();
		$start     = $data['start'];
		$length    = $data['length']+1;

		$sql .= " LIMIT {$start}, {$length}";
		// echo $sql;

		$list = $this->db->query($sql);

		$option = [
			'draw'            => $data['draw'],
			'recordsTotal'    => $rowCount,
			'recordsFiltered' => $filterRow,
			'data'            => [],
		];

		$no = $this->input->post('start', true) + 1;

		foreach ($list->result() as $row) {
			$rows = [
				'no'            => $no . '.',
				'id_pesanan'	=> $row->id_pesanan,
				'nomor_pesanan'	=> $row->nomor_pesanan,
				'nomor_meja'    => $row->nomor_meja,  
				'status'    	=> ($row->status == 1)?'<span class="label label-success">Selesai</span>':'<span class="label label-danger">Belum Dibayar</span>',  
				'aksi'    		=> '<buttton class="btn btn-primary edit btn-sm" data-id="'.$row->id_pesanan.'">Ubah</buttton> <buttton class="btn btn-danger hapus btn-sm" data-id="'.$row->id_pesanan.'">Hapus</buttton>',
			];
			$option['data'][] = $rows;
			$no++;
		}

		return $option;
	} 

	public function tambah($data, $rs){
		$this->db->trans_begin();
		$this->db->insert('pesanan', $data); 
		// print_r($this->db->last_query());  
		foreach ($rs as $key => $val) { 
			$this->db->insert('item_pesanan', $val); 
		}

		if ($this->db->trans_status() == FALSE) {
			$this->db->trans_rollback();
			return $this->db->error();
		} else {
			$this->db->trans_commit();
			return TRUE;
		}
	}

	public function edit($data, $rs)
	{  

		$this->db->trans_begin(); 

		$this->db->where('id_pesanan', $data['id_pesanan']);
		$this->db->update('pesanan', $data); 
		$this->db->delete('item_pesanan', array('id_pesanan' => $data['id_pesanan']));
		foreach ($rs as $key => $val) { 
			$this->db->insert('item_pesanan', $val); 
		}
		if ($this->db->trans_status() === false) {
			$this->db->trans_rollback(); 
			return false;
		} else {
			$this->db->trans_commit(); 
			return true;
		}
	}

	public function data_pesanan($param)
	{ 
		$query = $this->db->get_where('pesanan',array('id_pesanan' => $param));
		// print_r($this->db->last_query());    
		if ($query->num_rows() > 0) {
			return $query->row_array();
		}else{
			return [];
		}
	}

	public function data_menu_pesanan($param)
	{ 
		$query = $this->db->query("
			SELECT m.id_menu, m.nama_menu, m.harga, i.jumlah
			FROM item_pesanan i 
			LEFT JOIN menu m USING(id_menu)
			WHERE i.id_pesanan = ?
			", $param);

		if ($query->num_rows() > 0) {
			return $query->result_array();
		}else{
			return [];
		}
	}

	public function hapus($data)
	{

		$this->db->trans_begin(); 
		$this->db->delete('item_pesanan', array('id_pesanan' => $data));
		$this->db->delete('pesanan', array('id_pesanan' => $data));
		if ($this->db->trans_status() === false) {
			$this->db->trans_rollback(); 
			return false;
		} else {
			$this->db->trans_commit(); 
			return true;
		}
	}

	public function get_select2_menu()
	{
		$dt     = $this->input->post();
		$term   = "%" . $dt['q'] . "%";
		$params = [$term];
		$sql    = "SELECT id_menu as id , nama_menu as text, harga
		FROM menu
		";

		$sql .= ' HAVING text LIKE ?';


		$dt = $this->input->post();

		$data['total_record'] = $this->db->query($sql, $params)->num_rows();

		$start  = ($dt['page'] - 1) * $dt['page_limit'];
		$length = $dt['page_limit'];
		$sql .= " LIMIT {$start}, {$length}";

		$query = $this->db->query($sql, $params);

		$data['more'] = $data['total_record'] > $dt['page'] * $dt['page_limit'];

		if ($query->num_rows() > 0) {
			$result = $query->result_array();
			$query->free_result();
			$data['items'] = $result;
			return $data;
		} else {
			$data['items'] = [];
			return $data;
		}
	}

	public function get_last_pesanan()
	{
		$sql = "
		SELECT 
		*
		FROM pesanan 
		ORDER BY created_date DESC
		LIMIT 1
		";

		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			$result = $query->row_array();
			$query->free_result();
			return $result;
		} else {
			return null;
		}
	}

	public function data_pembayaran($param)
	{  

		$query = $this->db->query("
			SELECT p.*, u.nama
			FROM pembayaran p
			LEFT JOIN user u USING(id_user)
			WHERE p.id_pesanan = ?
			", $param);  
		// print_r($this->db->last_query());   
		if ($query->num_rows() > 0) {
			return $query->row_array();
		}else{
			return [];
		}
	}
}
