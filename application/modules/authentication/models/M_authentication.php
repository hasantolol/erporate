<?php

require_once APPPATH . 'models/M_model_base.php';

class M_authentication extends M_model_base {

    public function __construct() {
        parent::__construct();
    }

    function get_password_by_username($params) {
        $sql = "SELECT id, password, status FROM users WHERE username = '" . $params . "' ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    function get_detail_data_login($params) {
        $sql = "SELECT u.*, d.nama FROM users u
                LEFT JOIN desa_kelurahan d ON u.id_desa = d.id
                WHERE u.id = '" . $params . "'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

}