<?php
require_once APPPATH . 'controllers/Core_base.php';

class Authentication extends Core_base {

	public function __construct() {
        parent::__construct();
        $this->load->model('m_authentication');
    }

	public function index() {
        $this->load->view('index');
    }
	public function captcha(){
		$this->load->view('captcha');
	}
    function do_login() {
			if(isset($_POST["captcha"])&&$_POST["captcha"]!=""&&
			  $_SESSION["vercode"]==$_POST["captcha"]){
			  //Continue
			}
			else{
				$status['pesan'] = "Wrong verification code";
				$status['type'] = 'error';
				$status['status'] = false;
				$this->output->set_output(json_encode($status));
				return;
			}
        /* load form validation */
        $this->load->library('form_validation');
        /* set rule form validation */
        $this->form_validation->set_rules('username', 'Username', 'required|trim');
        $this->form_validation->set_rules('password', 'Password', 'required|trim');

        /* validasi form validation */
        if ($this->form_validation->run() === false) {
            $status['pesan'] = validation_errors();
            $status['type'] = 'error';
            $status['status'] = false;
            $this->output->set_output(json_encode($status));
            return;
        }

        /* ambil username and password */
        $username = $this->input->post('username', true);
        $password = $this->input->post('password', true) . $this->config->item('encryption_key');
        $key_pass = $this->m_authentication->get_password_by_username($username);
        // var_dump(password_verify($password, $key_pass['password']));exit();
        /* proses otentikasi */
        if (empty($key_pass) OR ! (password_verify($password, $key_pass['password']))) {
            /* jika user tidak ditemukan */
            $status['pesan'] = 'User atau password tidak ditemukan.';
            $status['type'] = 'error';
            $status['status'] = false;
            $this->output->set_output(json_encode($status));
            return;
        } else {
            /* validasi user memiliki status aktif | 1 = aktif | 0 = tidak aktif */
            if ($key_pass['status'] != 1) {
                // jika user tidak ditemukan
                $status['pesan'] = 'Akun Anda tidak aktif.  Silakan hubungi Administrator.';
                $status['type'] = 'error';
                $status['status'] = false;
                $this->output->set_output(json_encode($status));
                return;
            }
            /* ambil data login */
            $data_autentifikasi = $this->m_authentication->get_detail_data_login($key_pass['id']);
            $this->session->set_userdata('SESI_USER_LOGIN', $data_autentifikasi);
            $this->output->set_output(json_encode(array('status' => true)));
        }
    }

    function logout() {
        $data_login = $this->session->userdata('SESI_USER_LOGIN');
        $this->session->unset_userdata('SESI_USER_LOGIN');
        redirect('authentication');
    }

}
