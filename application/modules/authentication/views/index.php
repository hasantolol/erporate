<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Log in</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/backend/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/backend/adminlte/bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/backend/adminlte/bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/backend/adminlte/dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/backend/adminlte/plugins/iCheck/square/blue.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/jqueryformvalidator/form-validator/theme-default.min.css">

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  </head>
  <body>
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-9" style="background:url('<?=base_url()?>uploads/login-background.jpg'); height:100vh; background-size:100% 100%;">

      </div>
      <div class="col-sm-3" style="height:100vh; padding-top:60px; box-shadow: -5px 0px 10px 1px #000000;">
          <div class="login-logo">
            <b>Sign In / Masuk</b>
          </div>
          <!-- /.login-logo -->
          <div class="login-box-body">
            <form action="<?= base_url() ?>authentication" id="login-form" method="post" onsubmit="return false">
              <div class="form-group has-feedback">
                <input type="text" class="form-control" name="username" id="txt-username" placeholder="Username" data-validation="required">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
              </div>
              <div class="form-group has-feedback">
                <input type="password" class="form-control" name="password" placeholder="Password" data-validation="required">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
              </div>
              <div class="row" style="margin-bottom:10px;">
                <div class="col-xs-12">
                  <h5>Silahkan salin angka di bawah</h5>
                </div>
                <div class="col-xs-6">
                  <img src="<?=base_url()?>authentication/captcha" alt="Captcha Image"  data-validation="required" style="width:100%; height:40px;">
                </div>
                <div class="col-xs-6">
                  <input type="text" class="form-control text-center"  name="captcha" style="width:100%; height:40px; font-size:2em;">
                </div>
              </div>
              <div>
                <button type="submit" class="form-control btn btn-primary btn-flat" id="btn-login">Sign In</button>
              </div>
            </form>
            <!-- /.social-auth-links -->
          </div>
      </div>
    </div>
  </div>
  <!-- /.login-box -->
  <!-- jQuery 3 -->
  <script src="<?= base_url() ?>assets/backend/adminlte/bower_components/jquery/dist/jquery-2.1.1.min.js"></script>
  <script type="text/javascript">
    if (!window.jQuery) {
        document.write('<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"><\/script>');
    }
  </script>

  <!-- Bootstrap 3.3.7 -->
  <script src="<?= base_url() ?>assets/backend/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- iCheck -->
  <script src="<?= base_url() ?>assets/backend/adminlte/plugins/iCheck/icheck.min.js"></script>
  <script src="<?= base_url() ?>assets/plugins/jqueryformvalidator/form-validator/jquery.form-validator.min.js"></script>
  <script type="text/javascript" src="<?= base_url('assets/plugins/notification/SmartNotification.min.js') ?>"></script>
  <script>
    $(document).ready(function () {
      $('#txt-username').focus();
      $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' /* optional */
      });
      $('#login-form').on('submit', function () {
              if (!$(this).isValid()) {
                  return false;
              }
              var form = '#login-form';
              $.ajax({
                  type: 'post',
                  url: "<?php echo base_url('authentication/do_login'); ?>",
                  data: $(form).serialize(),
                  dataType: 'JSON',
                  timeout: 5000,
                  success: function (data) {
                      if (data.status) {
                          window.location.href = '<?= base_url() ?>dashboard';
                      } else {
                          $('#login-form').find("input[name=password]").val("");
                          $('#txt-password').focus();
                          show_alert('Kesalahan', data.pesan, data.type);
                      }
                  },
                  error: function (data) {
                      $('#login-form').find("input[name=password]").val("");
                      show_alert('Kesalahan', 'Tidak dapat mengakses ke server', 'error');
                  }
              });
          });
          // validasi form
          $.validate({
              lang: 'id'
          });
    });

    // alert
    function show_alert(title, content, type = 'info', time = 4000) {
        var warna;
        if (type == 'error')
            warna = '#C46A69';
        else if (type == 'info')
            warna = "#5384AF";
        $.smallBox({
            title: title,
            content: content,
            color: warna,
            timeout: time,
            icon: "fa fa-bell"
        });
    }

  </script>
  </body>
</html>
