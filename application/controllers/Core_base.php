<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Core_base extends MX_Controller {

	public $file_js = '';
    public $file_css = '';
	public $breadcrumb = '';
	public $com_user = array();

	public function __construct() {
        parent::__construct();
		$this->load->model('');
    }
	
    //fungsi untuk meload javascript
    protected function load_js($alamat) {
        if (is_file($alamat)) {
            $this->file_js .= '<script src="' . base_url($alamat) . '" type="text/javascript"></script>';
            $this->file_js .= "\n";
        } else {
            $this->file_js .= 'File javascript ' . $alamat . ' tidak ditemukan! <br>';
        }
    }

    //fungsi untuk meload css
    protected function load_css($alamat) {
        if (is_file($alamat)) {
            $this->file_css .= '<link href="' . base_url($alamat) . '" rel="stylesheet" type="text/css" />';
        } else {
            $this->file_css .= 'File css ' . $alamat . ' tidak ditemukan! <br>';
        }
    }

    // validasi login
    // protected function cek_login() {
    //     $this->com_user = $this->session->userdata('SESI_USER_LOGIN');
    //     if (empty($this->session->userdata('SESI_USER_LOGIN')))
    //         redirect('authentication');
    // }

    function is_login()
    {
        // $CI             = &get_instance();
        $this->com_user = $this->session->userdata('SESI_USER_LOGIN');
        if (empty($this->com_user)) {
            return false;
        } else {
            return true;
        }
    }

    //fungsi untuk menampilkan halaman
    protected function display($tpl_content = 'theme/default.php', $data = array(), $tpl_footer = NULL) {
        $data['FILE_JS'] = $this->file_js;
        $data['FILE_CSS'] = $this->file_css;
        $data['USER_LOGIN'] = $this->com_user;
        $data['TPL_ISI'] = $tpl_content;
        $data['CSRF_CONFIG'] = array('name' => $this->security->get_csrf_token_name(), 'hash' => $this->security->get_csrf_hash());
        $data['TPL_FOOTER'] = $tpl_footer;
        $this->load->view('theme/layout', $data);
    }

	public function tanggal_indo($tanggal, $cetak_hari = false){
		$hari = array ( 1 =>    'Senin',
					'Selasa',
					'Rabu',
					'Kamis',
					'Jumat',
					'Sabtu',
					'Minggu'
				);

		$bulan = array (1 =>   'Januari',
					'Februari',
					'Maret',
					'April',
					'Mei',
					'Juni',
					'Juli',
					'Agustus',
					'September',
					'Oktober',
					'November',
					'Desember'
				);
		$split 	  = explode('-', $tanggal);
		$tgl_indo = $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];

		if ($cetak_hari) {
			$num = date('N', strtotime($tanggal));
			return $hari[$num] . ', ' . $tgl_indo;
		}
		return $tgl_indo;
	}

}
