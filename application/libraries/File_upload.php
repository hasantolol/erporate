<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class File_upload {

    private $_ci;

    public function __construct() {
        $this->_ci = & get_instance();
    }

    public function unlink_file($file_location = null) {
        if ($file_location != null) {
            if (file_exists($file_location)) {
                unlink($file_location);
            }
        }
    }

    public function upload_single_file($file_url, $file_name) {
        $config["file_name"] = $file_name;
        $config['upload_path'] = $file_url;
        $config['allowed_types'] = 'gif|jpg|jpeg|xls|xlsx|pdf|png';
        /*
          $config['allowed_types']  = "exe|sql|psd|pdf|xls|ppt|php|php4|php3|js|swf|Xhtml|rar|zip|wav|bmp|gif|jpg|jpeg|png|html|htm|txt|rtf|mpeg|mpg|avi|doc|docx|xls|xlsx|csv";
          $config['max_size']       = "5000000";
          $config['max_width']      = "1200";
          $config['max_height']     = "1200";
         */
        $this->_ci->load->library("upload", $config);
        if (!$this->_ci->upload->do_upload()) {
            return $this->_ci->upload->display_errors();
        } else {
            return true;
        }
    }

    public function upload_multiple_file($file_url, $file_name, $count) {
        $result = '';

        $config['upload_path'] = $file_url;
        $config['allowed_types'] = 'gif|jpg|jpeg|png|xls|xlsx|pdf';
        $this->_ci->load->library("upload", $config);

        if ($count > 0) {
            for ($i = 0; $i < $count; $i++) {
                $_FILES['files']['name'] = $file_name[$i];
                $_FILES['files']['type'] = $_FILES['userfile']['type'][$i];
                $_FILES['files']['tmp_name'] = $_FILES['userfile']['tmp_name'][$i];
                $_FILES['files']['error'] = $_FILES['userfile']['error'][$i];
                $_FILES['files']['size'] = $_FILES['userfile']['size'][$i];

                $config["file_name"] = $file_name[$i];
                if (!$this->_ci->upload->do_upload('files')) {
                    $result .= 'Error file ' . $_FILES['userfile']['name'][$i] . ' gagal diupload <br/>';
                }
            }
        }

//        return $result;
    }

}

/* End of file file_upload.php */
/* Location: ./application/libraries/file_upload.php */
